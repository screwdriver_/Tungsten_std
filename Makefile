NAME = std.so
CC = g++
CFLAGS = -Wall -Wextra -Werror -fPIC

SRC_DIR := src/
SRC := $(shell find $(SRC_DIR) -type f -name "*.cpp")

INCLUDES := -I ../Tungsten/src

DIRS := $(shell find $(SRC_DIR) -type d)

OBJ_DIR := obj/
OBJ_DIRS := $(patsubst $(SRC_DIR)%, $(OBJ_DIR)%, $(DIRS))
OBJ := $(patsubst $(SRC_DIR)%.cpp, $(OBJ_DIR)%.o, $(SRC))

all: $(NAME)

mkdir:
	echo "Making object directories..."
	mkdir -p $(OBJ_DIRS)

$(NAME): mkdir $(OBJ)
	echo "Linking..."
	$(CC) $(CFLAGS) -shared -o $(NAME) $(OBJ) $(INCLUDES)

$(OBJ_DIR)%.o: $(SRC_DIR)%.cpp
	@echo "Compiling:" $<
	@$(CC) $(CFLAGS) -o $@ -c $< $(INCLUDES)

tags:
	ctags -R src/* --languages=c,c++

clean:
	echo "Cleaning objects..."
	rm -rf $(OBJ_DIR)

fclean: clean
	echo "Removing binaries..."
	rm -f $(NAME)

re: fclean all

.PHONY: all mkdir tags clean fclean re
.SILENT: all mkdir $(NAME) tags clean fclean re
