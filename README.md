Tungsten Standard Library
-------------------------

The Tungsten Standard Library (TSL) is a library provided by default with the Tungsten Virtual Machine (TVM).



# Compilation

Compilation instructions can be found on the [main Tungsten repository](https://gitlab.com/Crumble14/Tungsten/blob/master/README.md).



# Available natives

## Memory

- **malloc**: Allocates the specified number of bytes

Arguments:

| Name | Type   | Description                     |
|------|--------|---------------------------------|
| size | uint32 | The number of bytes to allocate |

Return: The pointer to the allocated memory



- **realloc**: Increases or decreases the size of the specified block of memory. Reallocates it if nedded

Arguments:

| Name    | Type   | Description                        |
|---------|--------|------------------------------------|
| pointer | void\* | The pointer to the block of memory |
| size    | uint32 | The number of bytes to reallocate  |

Return: The pointer to the reallocated memory



- **calloc**: Allocates the specified number of bytes and initializes them to zero

Arguments:

| Name   | Type   | Description                        |
|--------|--------|------------------------------------|
| number | uint32 | The number of elements to allocate |
| size   | uint32 | The size of each element           |

Return: The pointer to the allocated memory



- **free**: Releases the specified block of memory back to the system

Arguments:

| Name    | Type   | Description                        |
|---------|--------|------------------------------------|
| pointer | void\* | The pointer to the block of memory |

Return: void



## Utility

- **loadLibrary**: Loads the specified library

Arguments:

| Name | Type   | Description             |
|------|--------|-------------------------|
| path | char\* | The path to the library |

Return: void



- **createString**: Allocates a block of memory and moves a string from the stack to it

Arguments:

| Name   | Type | Description                                  |
|--------|------|----------------------------------------------|
| string | int8 | All the characters on the stack until ``\0`` |

Return: A pointer to the block of memory



## IO

- **printChar**: Prints a character into the terminal

Arguments:

| Name | Type | Description                 |
|------|------|-----------------------------|
| c    | int8 | The character to be printed |

Return: void



- **printInt**: Prints a 32 bits long integer into the terminal

Arguments:

| Name | Type  | Description               |
|------|-------|---------------------------|
| i    | int32 | The integer to be printed |

Return: void



- **printFloat**: Prints a floating-point integer into the terminal

Arguments:

| Name | Type  | Description                              |
|------|-------|------------------------------------------|
| f    | float | The floating-point integer to be printed |

Return: void



- **printBool**: Prints a boolean into the terminal

Arguments:

| Name | Type | Description               |
|------|------|---------------------------|
| b    | bool | The boolean to be printed |

Return: void



- **printString**: Prints a string from a pointer into the terminal

Arguments:

| Name | Type   | Description              |
|------|--------|--------------------------|
| s    | char\* | The string to be printed |

Return: void



- **println**: Prints a newline into the terminal

Arguments:

| Name | Type | Description |
|------|------|-------------|

Return: void



- **inputChar**: Reads a character from the terminal

Arguments:

| Name | Type | Description |
|------|------|-------------|

Return: The character which have been read



- **openFile**: Opens a file

Arguments:

| Name | Type   | Description                       |
|------|--------|-----------------------------------|
| path | char\* | The path to the file              |
| flag | int32  | The flag(s) for openning the file |

Return: The file descriptor



- **closeFile**: Closes a file

Arguments:

| Name | Type  | Description         |
|------|-------|---------------------|
| fd   | int32 | The file descriptor |

Return: Success code



- **mkdir**: Creates a new directory

Arguments:

| Name | Type   | Description                            |
|------|--------|----------------------------------------|
| path | char\* | The directory's path                   |
| flag | int32  | The flag(s) for creating the directory |

Return: Success code



- **rename**: Renames a file or directory

Arguments:

| Name      | Type   | Description         |
|-----------|--------|---------------------|
| old\_name | char\* | The file's old name |
| new\_name | char\* | The file's new name |

Return: Success code



- **remove**: Removes a file or directory

Arguments:

| Name | Type   | Description     |
|------|--------|-----------------|
| path | char\* | The file's path |

Return: Success code



- **openDir**: Opens a directory

TODO



- **closeDir**: Closes a directory

TODO



- **readDir**: Reads the next entry of an open directory

TODO



- **read**: Reads from a file descriptor

Arguments:

| Name   | Type   | Description                 |
|--------|--------|-----------------------------|
| fd     | int32  | The file descriptor         |
| buffer | void\* | The buffer                  |
| size   | int32  | The amount of bytes to read |

Return: The amount of bytes read



- **write**: Writes to a file descriptor

Arguments:

| Name   | Type   | Description                  |
|--------|--------|------------------------------|
| fd     | int32  | The file descriptor          |
| buffer | void\* | The buffer                   |
| size   | int32  | The amount of bytes to write |

Return: The amount of bytes written



## Math

- **sin**: Computes the sine function with the given value

Arguments:

| Name  | Type   | Description |
|-------|--------|-------------|
| value | double | The value   |

Result: The result of the function



- **cos**: Computes the cosine function with the given value

Arguments:

| Name  | Type   | Description |
|-------|--------|-------------|
| value | double | The value   |

Result: The result of the function



- **tan**: Computes the tangent function with the given value

Arguments:

| Name  | Type   | Description |
|-------|--------|-------------|
| value | double | The value   |

Result: The result of the function



- **asin**: Computes the arcsine function with the given value

Arguments:

| Name  | Type   | Description |
|-------|--------|-------------|
| value | double | The value   |

Result: The result of the function



- **acos**: Computes the arccosine function with the given value

Arguments:

| Name  | Type   | Description |
|-------|--------|-------------|
| value | double | The value   |

Result: The result of the function



- **atan**: Computes the arctangent function with the given value

Arguments:

| Name  | Type   | Description |
|-------|--------|-------------|
| value | double | The value   |

Result: The result of the function



- **exp**: Computes the exponential function with the given value

Arguments:

| Name  | Type   | Description |
|-------|--------|-------------|
| value | double | The value   |

Result: The result of the function



- **ln**: Computes the natural logarithm function with the given value

Arguments:

| Name  | Type   | Description |
|-------|--------|-------------|
| value | double | The value   |

Result: The result of the function



- **log**: Computes the logarithm function with the given value

Arguments:

| Name  | Type   | Description |
|-------|--------|-------------|
| value | double | The value   |

Result: The result of the function



- **pow**: TODO

TODO



- **sqrt**: Computes the square root of the given value

Arguments:

| Name  | Type   | Description |
|-------|--------|-------------|
| value | double | The value   |

Result: The square root of the value



- **cbrt**: Computes the cube root of the given value

Arguments:

| Name  | Type   | Description |
|-------|--------|-------------|
| value | double | The value   |

Result: The cube root of the value



- **floor**: Computes the floor function with the given value

Arguments:

| Name  | Type   | Description |
|-------|--------|-------------|
| value | double | The value   |

Result: The result of the function



- **ceil**: Computes the ceil function with the given value

Arguments:

| Name  | Type   | Description |
|-------|--------|-------------|
| value | double | The value   |

Result: The result of the function



### Thread

- **threadGetId**: Returns the id of the current thread

Arguments:

| Name | Type | Description |
|------|------|-------------|

Result: The id of the current thread



- **threadCreate**: Creates a new thread

Arguments:

| Name      | Type   | Description                                              |
|-----------|--------|----------------------------------------------------------|
| begin     | uint32 | The beginning instruction of the function for the thread |
| parameter | void*  | A parameter to be pushed to the thread's stack           |

Result: The id of the created read



- **threadSleep**: Makes the thread sleep for the specified delay

Arguments:

| Name  | Type   | Description                                           |
|-------|--------|-------------------------------------------------------|
| delay | uint32 | The delay the thread will sleep for (in milliseconds) |

Result: void



- **threadExit**: Exits the current thread

Arguments:

| Name | Type | Description |
|------|------|-------------|

Result: void



- **threadKill**: Kills the specified thread

Arguments:

| Name | Type   | Description     |
|------|--------|-----------------|
| id   | uint32 | The thread's id |

Result: void
