@include "io.w"
@include "../string.w"

@define STDIN	0
@define STDOUT	1
@define STDERR	2

namespace std
{
	int8 term_read()
	{
		int8 buffer
		read($STDIN, &buffer, sizeof(buffer))

		return buffer;
	}

	void term_write(const int8 c)
	{
		write($STDOUT, &c, sizeof(c))
	}

	void term_write(const int8* str)
	{
		write($STDOUT, str, strlen(str))
	}

	void term_write(const int32 n)
	{
		// TODO
	}

	void term_write(const float n)
	{
		// TODO
	}
}
