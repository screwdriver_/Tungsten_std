namespace std
{
	int32 read(const int32 fd, int8* buffer, const uint32 size)
	{
		asm {
			vload4 0
			vload8 4
			vload4 12
			ncall read
		}
	}

	int32 write(const int32 fd, int8* buffer, const uint32 size)
	{
		asm {
			vload4 0
			vload8 4
			vload4 12
			ncall write
		}
	}
}
