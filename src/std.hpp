#ifndef TUNGSTEN_STD_HPP
# define TUNGSTEN_STD_HPP

# include <chrono>
# include <cstdlib>
# include <thread>

# include <fcntl.h>
# include <math.h>
# include <pthread.h>
# include <signal.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <time.h>
# include <unistd.h>

# include <tungsten.hpp>

namespace Tungsten_std
{
	using namespace Tungsten;

	// ----------
	//    memory
	// ----------

	void n_stack_size(VM&, Thread& thread);
	void n_stack_reserve(VM&, Thread& thread);

	void n_callstack_depth(VM&, Thread& thread);
	void n_callstack_reserve(VM&, Thread& thread);

	void n_malloc(VM&, Thread& thread);
	void n_realloc(VM&, Thread& thread);
	void n_free(VM&, Thread& thread);

	// ----------
	//    util
	// ----------

	void n_load_library(VM& vm, Thread& thread);

	// ----------
	//    io
	// ----------

	void n_open(VM&, Thread& thread);
	void n_close(VM&, Thread& thread);

	void n_read(VM&, Thread& thread);
	void n_write(VM&, Thread& thread);

	void n_mkdir(VM&, Thread& thread);
	void n_rename(VM&, Thread& thread);
	void n_remove(VM&, Thread& thread);

	void n_opendir(VM&, Thread& thread);
	void n_closedir(VM&, Thread& thread);

	void n_readdir(VM&, Thread& thread);

	// ----------
	//    math
	// ----------

	void n_sin(VM&, Thread& thread);
	void n_cos(VM&, Thread& thread);
	void n_tan(VM&, Thread& thread);

	void n_asin(VM&, Thread& thread);
	void n_acos(VM&, Thread& thread);
	void n_atan(VM&, Thread& thread);

	void n_exp(VM&, Thread& thread);
	void n_ln(VM&, Thread& thread);
	void n_log(VM&, Thread& thread);

	void n_pow(VM&, Thread& thread);
	void n_sqrt(VM&, Thread& thread);
	void n_cbrt(VM&, Thread& thread);

	void n_floor(VM&, Thread& thread);
	void n_ceil(VM&, Thread& thread);

	// ----------
	//    thread
	// ----------

	void n_thread_get_id(VM&, Thread& thread);
	void n_thread_create(VM& vm, Thread& thread);
	void n_thread_sleep(VM&, Thread& thread);
	void n_thread_exit(VM&, Thread& thread);
	void n_thread_kill(VM&, Thread& thread);
}

extern "C" void init(Tungsten::VM& vm);

#endif
