@define NULL	0

namespace std
{
	void* malloc(const uint32 size)
	{
		void* p

		asm {
			vload4 0
			ncall malloc
			vstore8 4
		}

		return (p)
	}

	void* realloc(void* ptr, const uint32 size)
	{
		void* p

		asm {
			vload8 0
			vload4 8
			ncall realloc
			vstore8 4
		}

		return (p)
	}

	void free(void* ptr)
	{
		asm {
			vload8 0
			ncall free
		}
	}

	void* memcpy(void* dest, const void* src, const uint32 size)
	{
		if(dest == $NULL | src == $NULL) {
			return (dest)
		}

		for(uint32 i = 0, (i < size), ++i) {
			dest[i] = src[i]
		}

		return (dest)
	}
}
