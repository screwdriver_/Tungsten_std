#include "std.hpp"

void Tungsten_std::n_stack_size(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push<uint64_t>(stack.size());
}

void Tungsten_std::n_stack_reserve(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	const auto size = stack.get<uint64_t>();

	stack.reserve(size);
}

void Tungsten_std::n_callstack_depth(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push<uint64_t>(thread.get_call_depth());
}

void Tungsten_std::n_callstack_reserve(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	const auto size = stack.get<uint64_t>();

	thread.get_callstack().reserve(size * sizeof(uint32_t) * 2);
}

void Tungsten_std::n_malloc(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(malloc(stack.get<uint32_t>()));
}

void Tungsten_std::n_realloc(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	const auto size = stack.get<uint32_t>();
	const auto pointer = stack.get<void*>();

	stack.push(realloc(pointer, size));
}

void Tungsten_std::n_free(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	free(stack.get<void*>());
}
