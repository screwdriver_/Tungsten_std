#include "std.hpp"

void Tungsten_std::n_load_library(VM& vm, Thread& thread)
{
	auto& stack = thread.get_stack();
	const string path(reinterpret_cast<char*>(stack.get<intptr_t>()));

	vm.load_library(path);
}

